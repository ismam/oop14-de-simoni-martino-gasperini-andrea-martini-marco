package esseri;

/**
 * @author Andrea
 * 
 *         Difensivo � una classe astratta che contiene metodi usabili nelle
 *         classi che la estendono (es. Noce) ======= Difensivo è una classe
 *         astratta che contiene metodi usabili nelle classi che la estendono
 *         (es. Noce) >>>>>>> refs/remotes/default/master
 *
 */
// classe astratta difensivo che implementa i metodi delle piante difensive del
// programma
public abstract class Difensivo extends Pianta implements IDifensivo {

	public Difensivo(final String path, final Double _life,
			final Double _danno, final Integer _tempo_richiesto,
			final TipoTerreno _terreno) {

		super(path, _life, _danno, _tempo_richiesto, _terreno);
	}

}
