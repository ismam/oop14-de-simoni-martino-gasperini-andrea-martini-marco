package esseri;

import gui.NessunImmagine;

import java.awt.image.BufferedImage;

import javax.swing.JPanel;

import controller.AbstractInsertionPanelController;


public class NessunEssere implements IEssere {

	
	private static NessunEssere nessunEssere = new NessunEssere();
	
	private NessunEssere(){};
	
	public static NessunEssere getInstance(){
		
		return nessunEssere;
		
	}


	@Override
	public TipoEssere getTipoEssere() {
		return null;
	}

	@Override
	public TipoTerreno getTerreno() {
		return null;
	}

	@Override
	public void prendiDanno(double danno) {
		
	}

	@Override
	public void eseguireAllaMorte() {
		
	}

	@Override
	public int getTempoRichiesto() {
		return 0;
	}

	@Override
	public int getTempoTrascorso() {
		return 0;
	}

	@Override
	public void inviaAzione(int tempo_trascorso,
			AbstractInsertionPanelController<Azione, ? extends JPanel> controller,
			Posizione2D pos) {
		
	}

	@Override
	public BufferedImage getImg() {
		return NessunImmagine.getInstance();
	}

	@Override
	public boolean isDead() {
		return false;
	}
	
	
}
