/**
 * 
 */
package esseri;


import java.awt.image.BufferedImage;



/**
 * @author Andrea
 * @author Marco
 * @author Martino
 */
public abstract class Essere implements IEssere {

    protected int tempo_trascorso=0;
	final protected Integer tempo_richiesto;
    protected double life;
	public final TipoEssere nome;
	protected final Double danno;
	protected final BufferedImage img;
	protected final TipoTerreno terreno;
	
	//Costruttori
	
	//TODO avvicinare i costruttori delle altre classi a questo tipo 
	public Essere( final String path, final TipoEssere _nome, final Double _life, final Double _danno, 
			final Integer _tempo_richiesto, final TipoTerreno _terreno ){
		
		nome = _nome;
		terreno = _terreno;
		img = gui.Utility.initImg(path);

		life = new Double ( _life.doubleValue());
		danno = new Double (_danno.doubleValue());
		tempo_richiesto = new Integer( _tempo_richiesto.intValue());
	}
	
	protected boolean canAct(int tempo){ // side effect su tempo_in_ms
		
		this.tempo_trascorso+=tempo;
		if(tempo_trascorso>=tempo_richiesto){
			this.tempo_trascorso-=tempo_richiesto;
			return true;
		}
		return false;
	}
	
	public double getLife(){
	return this.life;	
	}

	public int getTempoTrascorso() {
		return tempo_trascorso;
	}

	public int getTempoRichiesto(){
		return tempo_richiesto;
	}
	
	public TipoEssere getTipoEssere(){
		return this.nome;
	}
	
	@Override
	public BufferedImage getImg() {
		return img;
	}

	
	
}
