package esseri;


/*classe astratta che mi definisce tutte le caratteristiche comuni a tutti gl zombi
 *  
 * 
 */

/**
 * Astratta di ogni zombi
 * 
 * @author Martino De Simoni
 * @author Marco Martini
 */

public abstract class Zombi extends Essere implements IZombi {

	// Setto il tipoTerreno a cortile tanto la maggior parte delgi zombi è li
	// per gli zombi che non sono
	// del cortile utilizzo lo implemento nelle classi concrete degli zombi

	protected TipoTerreno tipoTerreno = TipoTerreno.CORTILE;

	protected static final String cartellaImmaginiZombi = "imgZombi/" ; // infallibile

	public Zombi(final String imgName, final Double _life, final Double _danno,
			final Integer _tempo_richiesto, final TipoTerreno terreno) {

		super(cartellaImmaginiZombi + imgName, TipoEssere.ZOMBIE, _life,
				_danno, _tempo_richiesto, terreno);
	}

	public void prendiDanno(double danno) {

		super.life -= danno;

	}

	// algoritmo di fai robe da finire

	public TipoTerreno getTerreno() {

		return this.tipoTerreno;

	}

	public double getLife() {

		return this.life;
	}

	public double getDanno() {

		return super.danno;
	}

	public void eseguireAllaMorte() {
	}

	public boolean isDead() {

		return this.life <= 0;
	}

}
