package esseri;

/**
 * @author Andrea Support � un'astrata che contiene metodi per le piante di
 *         supporto.
 *
 */
// astratta supporto che implementa i metodi delle piante di supporto del
// programma
public abstract class Support extends Pianta implements ISupport {

	boolean hoattaccato = false;

	public Support(final String path, final Double _life, final Double _danno,
			final Integer _tempo_richiesto, final TipoTerreno _terreno) {

		super(path, _life, _danno, _tempo_richiesto, _terreno);
	}
}