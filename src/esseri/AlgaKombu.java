/**
 * 
 */
/**
 * 
 */
package esseri;

import javax.swing.JPanel;

import controller.AbstractInsertionPanelController;

/**
 * @author Andrea Questa pianta , � un'alga acquatica, che pu� essere giocata
 *         solo in acqua , e una volta messa in gioco non fa nulla se non �
 *         attaccata, ma quando viene attaccata, infligge un danno elevato allo
 *         zombie.
 *
 */
public class AlgaKombu extends Support {

	private static String img_filePath = "algakombu.jpe";

	public AlgaKombu() {

		super(img_filePath, Utility.PIANTA_VITA_BASSA,
				Utility.PIANTA_DANNO_ALTO, 0, TipoTerreno.ACQUA);

	}

	public void inviaAzione(
			int tempo_trascorso,
			AbstractInsertionPanelController<Azione, ? extends JPanel> controller,
			Posizione2D posizione) {
		if (canAct(tempo_trascorso)) {

			if (attaccato == true && hoattaccato == false) {
				controller.insert(new Attacco(TipoEssere.ZOMBIE,
						new Posizione2D(0, 0), danno, NessunAzione
								.getInstance(), posizione));
				controller.insert(new Attacco(TipoEssere.ZOMBIE,
						new Posizione2D(1, 0), danno, NessunAzione
								.getInstance(), posizione));
				controller.insert(new Attacco(TipoEssere.ZOMBIE,
						new Posizione2D(-1, 0), danno, NessunAzione
								.getInstance(), posizione));

				hoattaccato = true;
			}

			else if (attaccato && hoattaccato) {
				controller.insert(new Attacco(TipoEssere.PIANTA,
						new Posizione2D(0, 0), this.life, NessunAzione
								.getInstance(), posizione));
			}
		}
	}

	@Override
	public int getSoliRichiesti() {

		return 25;
	}

	@Override
	public TipoTerreno getTerreno() {

		return TipoTerreno.ACQUA;
	}

	@Override
	public void eseguireAllaMorte() {

	}

	@Override
	public boolean isDead() {
		return hoattaccato;
	}

}
