package esseri;




/**
 * @author Andrea Pianta � un essere come Pallino e Zombie ,� un'astratta che
 *         contiene i metodi utili a tutte le piante del programma.
 * 
 */
public abstract class Pianta extends Essere implements IPianta {

	// public double life;
	public int tempo_in_ms;
	public int costo; // costo della piante
	private TipoTerreno terra;
	boolean attaccato = false;
	protected static final String cartellaImmaginiPiante = "imgPiante/" ; // infallibile

	public TipoEssere tipo = TipoEssere.PIANTA;

	public Pianta(final String path, final Double _life, final Double _danno,
			final Integer _tempo_richiesto, final TipoTerreno _terreno) {

		super( cartellaImmaginiPiante+path, TipoEssere.PIANTA, _life, _danno,
				_tempo_richiesto, _terreno);

	}

	public TipoTerreno getTerrenoAccettabile() {
		return this.terra;
	}

	public void cambia_immagine() {
	}

	public void muore() {
	}

	public int getCosto() {
		return costo;
	}

	public void prendiDanno(double d) {

		attaccato = true;
		super.life -= d;
	}

}