package controller;

import file_manager.Giocatore;
import gui.MenuPanel;

/**
 * 
 * 
 * Controller del pannello del men�.
 * 
 * @author Martino De Simoni
 */
/*
 * 
 * La classe implementa il pattern mvc e compone la view.
 * La presente � una classe totalmente "stupida", che definisce la sola grafica relegando al controller quasi qualsiasi azione 
 * di logica.
 *
 * Nonostante sia stata disegnata ad hoc, la classe � in buona parte riutilizzabile.
 *
 */

public class MenuController extends PanelController<MenuPanel> {
		
	private final Giocatore g;
	
	
	//Stringhe per farsi notificare dal controlledPanel
	private final String gameMessage;
	private final String optionsMessage;
	private final String userChoiceMessage;
	private final String exitMessage;
	//Stringhe per notificare il master
	
	/**
	 * Inizializza lo stato del controller.
	 * 
	 * @param _g Il giocatore di cui vengono visualizzati i dati
	 * @param termination La stringa da dare come argomento a notifyMaster() per notificare la propria terminazione.
	 * @param game Stringa che il pannello associato ritorner� a notifyController(). Il controller notificher� al master.
	 * @param options Stringa che il pannello associato ritorner� a notifyController(). Il controller notificher� al master.
	 * @param userChoice Stringa che il pannello associato ritorner� a notifyController(). Il controller notificher� al master.
	 * @param exit Stringa che il pannello associato ritorner� a notifyController(). Il controller notificher� al master.
	 * @param _master Il master (consiglio: se si usa il costruttore da master, usare "this")
	 */
	
	public MenuController(final Giocatore _g,final String termination,final String game,final String options,
											final String userChoice,final String exit, final AbstractMasterPanelController _master ){
		
		g = _g;
		gameMessage = game;
		optionsMessage = options;
		userChoiceMessage = userChoice;
		panelID = termination;
		exitMessage = exit;
		
		master = _master;
		
		controlledPanel = new MenuPanel( game, options, userChoice, exit, this, g, gui.Utility.SFONDO );

	}

	
	@Override
	public void slaveHasTerminated() {
		
		this.controlledPanel.setVisible(false); // questo pannello resta in memoria
		
	}

	@Override
	public void notifyController(String msg) {
		
		if( msg == gameMessage){
			
			master.notifyMaster(panelID, gameMessage );
			
		}
		else if( msg == optionsMessage){
			
			master.notifyMaster(panelID, optionsMessage );

		}
		else if( msg == userChoiceMessage){
			
			master.notifyMaster(panelID, userChoiceMessage );

			
		}
		
		else if(msg == exitMessage){
			
			master.notifyMaster(panelID, exitMessage );
		}
		
	}

	@Override
	public void run() {
		this.controlledPanel.setVisible(true);
		this.controlledPanel.update(this.controlledPanel.getGraphics());
	}
	
}
