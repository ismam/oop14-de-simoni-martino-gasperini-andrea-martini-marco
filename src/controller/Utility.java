package controller;

import java.io.File;

/**
 * 
 * La presente � una classettona di costanti public final static e di metodi
 * static vari che possono essere usate, anche solo idealmente, in DUE O PIU'
 * classi.
 * 
 * La classe � ottima per il debug, la stesura del codice e per la traduzione da
 * lingua a lingua.
 * 
 * L'utilizzo di questa classe espone il programma a due problemi:
 * 
 * - A tempo di run, un oggetto A potrebbe utilizzare un oggetto X qui definito,
 * e B dovrebbe utilizzare lo stesso oggetto di A, cambiando oggetto in uso al
 * variare dell'oggetto di A, mentre invece utilizza lo stesso oggetto della
 * classe utility. Nel caso A e B non cambino oggetto, il programmatore che
 * mantiene il codice, dovendo cambiare solo quello di A o solo di B, dovr� fare
 * attenzione.
 * 
 * - Durante il mantenimento, nel caso in cui la stessa immagine abbia due
 * funzioni diverso ( per esempio, usare il logo anche come sfondo), utilizzando
 * la stessa etichetta "logo", nel momento in cui si vorr� cambiare lo sfondo,
 * si cambier� anche il logo. Un po' come associare il contatore di un ciclo e
 * quello dei thread utilizzati allo stesso int: insensato. Le variabili devono
 * essere definite per semantica e non per valore.
 *
 * In un ambiente di buona programmazione mi sento di raccomandare questa
 * classe.
 * 
 * @author Martino De Simoni
 *
 * */

public final class Utility {

	public final static String FILE_DATI_UTENTE = System
			.getProperty("user.dir")
			+ File.separatorChar
			 + "utenti.txt";

	public final static String FILE_LIVELLI = System.getProperty("user.dir")
			+ File.separatorChar
			 + "livelli.txt";

	public final static Integer TICK = Integer.valueOf(100);
	public final static Integer SOLI_A_INIZIO_LIVELLO = 40000;

	private Utility() {
	};

	/**
	 * 
	 * Un metodo lentissimo da usare con cura.
	 * 
	 * @param s
	 *            Un array di String
	 * @return Lo stesso array di String sottoforma di unica stringa
	 */
	public static String stringArrayToString(final String[] s) {

		String daRitornare = new String("");
		for (final String t : s) {
			daRitornare = new String(daRitornare.concat(t + " "));
		}

		return daRitornare;

	}

	/*
	 * tokenToName lo preferisco qui che nel package dei file, il nome deve
	 * essere visualizzato allo stesso modo in tutta la GUI.
	 */

	/**
	 * 
	 * @param s
	 *            Il token letto da file
	 * @return Il nome come deve essere verosimilmente visualizzato. La javadoc
	 *         di UserDataManager.nameToToken(String) descrive la perdita di
	 *         dati.
	 * 
	 */
	public static String tokenToName(final String s) {

		if (s == null) {
			return null;
		}
		return s.replace("_", " ");

	}

}
