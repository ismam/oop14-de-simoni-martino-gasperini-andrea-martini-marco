package gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import controller.PanelController;
import file_manager.Giocatore;

/**
 * 
 *
 * Da questo pannello si decide a quale altro dei pannelli passare, come in qualsiasi men� di gioco.
 *
 * @author Martino De Simoni 
 */
/*
 * La classe implementa il pattern mvc e compone la view.
 * La presente � una classe totalmente "stupida", che definisce la sola grafica relegando al controller quasi qualsiasi azione 
 * di logica.
 *
 * Eccezion fatta per i bottoni, la classe � totalmente riutilizzabile.
 * 
 */

public class MenuPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	
	private final Giocatore g;
	private final BufferedImage background;
	//Bottoni
	private final JButton campaign;
	private final JButton options = new JButton (Utility.OPTIONS);
	private final JButton backToUserChoice = new JButton (Utility.BACK_TO_USER_CHOICE);
	private final JButton exit = new JButton (Utility.SAVE_AND_EXIT);
	private JLabel dati = new JLabel(); //NON deve essere final, i dati cambiano nel tempo
	//Stringhe per notificare il controller
	private final String gameMessage;
	private final String optionsMessage;
	private final String userChoiceMessage;
	private final String exitMessage;
	//Controller
	private final PanelController<MenuPanel> controller;

	
/**
 * 
 * Metodo preso da http://www.simplesoft.it/background_image_per_componenti_java_swing.html
 * 
 */


 protected void paintComponent(final Graphics g) {

	    super.paintComponent(g);
	    
	    g.drawImage(background, 0, 0, getWidth(), getHeight(), this);
	    
 	}

	public MenuPanel(final String _game, final String _options, final String _userChoice, final String _exit,
						final PanelController<MenuPanel> _controller, final Giocatore _g, final BufferedImage _background){
		
		//Inizializzazione campi
		gameMessage = _game;
		optionsMessage = _options;
		userChoiceMessage = _userChoice;
		exitMessage = _exit;
		
		controller = _controller;
		
		g=_g;
		
		background = _background;

		this.setLayout( new BorderLayout() );
		
		JPanel buttonsPanel = new JPanel( new FlowLayout() );
		buttonsPanel.setOpaque(false);
		//Inizializzazione bottoni
		
		
		campaign = new JButton (Utility.PLAY_CAMPAIGN+" Livello "+this.g.livello);
		
		buttonsPanel.add(campaign);
		buttonsPanel.add(options);
		buttonsPanel.add(backToUserChoice);
		buttonsPanel.add(exit);
		
		this.add(buttonsPanel,BorderLayout.NORTH);
		
		campaign.addActionListener(e-> {
		       this.controller.notifyController( gameMessage );
		      });
			
		options.addActionListener(e-> {
			   this.controller.notifyController( optionsMessage );
		});
			
		backToUserChoice.addActionListener(e-> {	
			   this.controller.notifyController( userChoiceMessage );
		});
	
		exit.addActionListener(e-> {	
			   this.controller.notifyController( exitMessage );
		});
		
		dati = new JLabel(Utility.NAME + ": " + g.nome + "  " 
				  + Utility.MONEY + ": " + g.soldi);


dati.setHorizontalAlignment(this.getWidth()/2);

this.add(dati,BorderLayout.CENTER);

		
	}
	
}
