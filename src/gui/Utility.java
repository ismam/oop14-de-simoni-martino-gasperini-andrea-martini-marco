package gui;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.*;

import javax.imageio.ImageIO;

/*
 * I campi sono inizializzati in italiano. Sarebbe bello un metodo che, presa in input una lingua, li inizializzasse in tale lingua.
 * 
 */

/**
 * 
 * La presente � una classettona di costanti public final static e di metodi
 * static vari che possono essere usate, anche solo idealmente, in DUE O PIU'
 * classi.
 * 
 * La classe � ottima per il debug, la stesura del codice e per la traduzione da
 * lingua a lingua.
 * 
 * L'utilizzo di questa classe espone il programma a due problemi:
 * 
 * - A tempo di run, un oggetto A potrebbe utilizzare un oggetto X qui definito,
 * e B dovrebbe utilizzare lo stesso oggetto di A, cambiando oggetto in uso al
 * variare dell'oggetto di A, mentre invece utilizza lo stesso oggetto della
 * classe utility. Nel caso A e B non cambino oggetto, il programmatore che
 * mantiene il codice, dovendo cambiare solo quello di A o solo di B, dovr� fare
 * attenzione.
 * 
 * - Durante il mantenimento, nel caso in cui la stessa immagine abbia due
 * funzioni diverso ( per esempio, usare il logo anche come sfondo), utilizzando
 * la stessa etichetta "logo", nel momento in cui si vorr� cambiare lo sfondo,
 * si cambier� anche il logo. Un po' come associare il contatore di un ciclo e
 * quello dei thread utilizzati allo stesso int: insensato. Le variabili devono
 * essere definite per semantica e non per valore.
 *
 * In un ambiente di buona programmazione mi sento di raccomandare questa
 * classe.
 * 
 * @author Martino De Simoni
 *
 * */

public final class Utility {

	private Utility() {
	};

	public static final String CARTELLA_IMMAGINI = 
			 "img/";

	public static final boolean FULLSCREEN = true;
	public static final int VERTICAL_RAZIO_WHEN_NOT_FULLSCREEN = 2;
	public static final int ORIZONTAL_RAZIO_WHEN_NOT_FULLSCREEN = 2;

	public static final BufferedImage LOGO = initImg(Utility.CARTELLA_IMMAGINI+"logo.jpg");
	public static final BufferedImage SFONDO = initImg(Utility.CARTELLA_IMMAGINI +"sfondo.jpg");
	public static final BufferedImage SFONDO_IN_CARICAMENTO = initImg(Utility.CARTELLA_IMMAGINI +"loading.jpg");
	public static final BufferedImage PALA = initImg(Utility.CARTELLA_IMMAGINI +"pala.jpg");
	public static final BufferedImage SOLE = initImg(Utility.CARTELLA_IMMAGINI +"sun.jpg");
	public static final BufferedImage ERBA = initImg(Utility.CARTELLA_IMMAGINI +"grass.jpg");
	public static final BufferedImage ACQUA = initImg(Utility.CARTELLA_IMMAGINI +"swimmingPool.jpe");
	// Stringhe per i bottoni
	public static final String ADD_USER = "Nuovo";
	public static final String REMOVE_USER = "Rimuovi";
	public static final String SELECT_USER = "Seleziona";

	public final static String CLOSE = "Chiudi";
	public final static String CANCEL = "Annulla";
	public static final String EXIT = "Esci";
	public static final String SAVE_AND_EXIT = "Salva ed esci";

	public static final String CHOOSE = "Scegli le tue piante!";
	public static final String PLAY = "Gioca!";
	public static final String PLAY_CAMPAIGN = "Campagna";
	public static final String OPTIONS = "Opzioni";
	public static final String BACK_TO_USER_CHOICE = "Torna alla scelta utente";
	// Stringhe per il resto della gui
	public final static String TITLE = "Piante contro Zombie";
	public final static String ADD_USER_QUESTION = "Inserisci il nome del nuovo utente";
	public final static String MESSAGE_FOR_EXIT = "Sicuro di voler uscire (i dati non salvati andranno persi) ?";
	public final static String USER_ALREADY_EXISTS_ERROR = "Errore: nome utente gi� selezionato";

	public static final String NAME = "Nome";
	public static final String MONEY = "Soldi";

	public static final String DEFEAT = "Peccato! Hai perso, ma sarai pi� fortunato la prossima volta";
	public static final String VICTORY = "Hai vinto!";

	/**
	 * 
	 * @param fileName
	 *            Percorso del file da cui prendere l'immagine. (barra iniziale
	 *            gi� aggiunta)
	 * @return L'immagine.
	 */

	public static BufferedImage initImg(String fileName) {
		BufferedImage img = null;

		try {

			img = ImageIO.read(Utility.class.getClassLoader()
					.getResourceAsStream(  fileName ));
		} catch (IOException e) {
			e.printStackTrace();
		}

		return img;
	}

	/*
	 * Metodo preso da
	 * http://stackoverflow.com/questions/15558202/how-to-resize-image-in-java
	 */
	/**
	 * 
	 * @param img
	 *            L'immagine di cui si vuole fare il resize
	 * @param newW
	 *            La nuova larghezza
	 * @param newH
	 *            La nuova altezza
	 * @return L'immagine ridimensionata
	 * @author 
	 *         http://stackoverflow.com/questions/15558202/how-to-resize-image-in
	 *         -java
	 */
	public static BufferedImage resizeImg(BufferedImage img, int newW, int newH) {

		int w = img.getWidth();
		int h = img.getHeight();
		BufferedImage dimg = new BufferedImage(newW, newH, img.getType());
		Graphics2D g = dimg.createGraphics();
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
				RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g.drawImage(img, 0, 0, newW, newH, 0, 0, w, h, null);
		g.dispose();
		return dimg;
	}

	/**
	 * Prende in input due immagini e le unisce in un'unica immagine
	 * 
	 * @author http://stackoverflow.com/questions/2318020/merging-two-images
	 * @param image
	 *            L'immagine a cui verr� sovrapposta la seconda.
	 * @param overlay
	 *            L'immagine che sar� sovrapposta alla prima.
	 * @return La combinazione delle due immagini.
	 */

	public static BufferedImage mergeImage(BufferedImage image,
			BufferedImage overlay) {
		// create the new image, canvas size is the max. of both image sizes
		int w = Math.max(image.getWidth(), overlay.getWidth());
		int h = Math.max(image.getHeight(), overlay.getHeight());
		BufferedImage combined = new BufferedImage(w, h,
				BufferedImage.TYPE_INT_ARGB);

		// paint both images, preserving the alpha channels
		Graphics g = combined.getGraphics();
		g.drawImage(image, 0, 0, null);
		g.drawImage(overlay, 0, 0, null);

		return combined;

	}

	// TODO esiste un modo migliore di implementare questo metodo
	public static boolean isAValidImage(final BufferedImage img) {

		if (img == null || img == NessunImmagine.getInstance())
			return false;
		return true;
	}

}